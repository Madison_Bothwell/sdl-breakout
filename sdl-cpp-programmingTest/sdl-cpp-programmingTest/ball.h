#pragma once
#include <SDL.h>
#include "entity.h"

class Ball : public Entity
{
private:
	int xVelocity, yVelocity;
	void checkBounds();
public:
	Ball();
	Ball( int x, int y, SDL_Texture* texture, int xVelocity, int yVelocity );
	int getXVelocity();
	void setXVelocity( int xVelocity );
	int getYVelocity();
	void setYVelocity( int yVelocity );
	void update();
	void switchXDirection();
	void switchYDirection();
	bool isStationary();
};

