#pragma once
#include <SDL.h>
#include "entity.h"

class Paddle : public Entity
{
private:
	void checkBounds();
public:
	Paddle();
	Paddle( int x, int y, SDL_Texture* texture );
	void update();
};

