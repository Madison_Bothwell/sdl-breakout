#pragma once
#include <SDL.h>
#include <string>
#include "entity.h"

const int SCREEN_HEIGHT = 500;
const int SCREEN_WIDTH = 640;
const int SCREEN_CENTER_X = SCREEN_WIDTH / 2;

class RenderWindow
{
public:
	RenderWindow();
	RenderWindow( const char* windowTitle );
	SDL_Texture* loadTexture( const std::string& file );
	void renderTexture( SDL_Texture* texture, int x, int y );
	void render( Entity& entity );
	void cleanUp();
	void clear();
	void display();
private:
	SDL_Window* window;
	SDL_Renderer* renderer;
};

