#include "game.h"

const int INIT_BALL_SPEED = 5;
const int MAX_PADDLE_X_REBOUND = 5;
const int NUM_BRICK_ROWS = 4;

Game::Game(RenderWindow& gameWindow):
	gameWindow(gameWindow)
{
}

void Game::initGame()
{
	SDL_Texture* ballTexture = gameWindow.loadTexture( "res/ball.bmp" );
	SDL_Texture* paddleTexture = gameWindow.loadTexture( "res/paddle.bmp" );
	SDL_Texture* brickTexture = gameWindow.loadTexture( "res/brick.bmp" );

	paddle = Paddle( SCREEN_CENTER_X - paddle.getWidth() / 2, SCREEN_HEIGHT - paddle.getHeight() - 10, paddleTexture );

	ball = Ball( SCREEN_CENTER_X - ball.getWidth() / 2, paddle.getY() - ball.getHeight(), ballTexture, 0, 0 );

	bricks = std::vector<Brick>();
	int width, height;
	SDL_QueryTexture( brickTexture, NULL, NULL, &width, &height );
	for( int i = 0; i < SCREEN_WIDTH / width; i++ ) {
		for( int j = 0; j < NUM_BRICK_ROWS; j++ ) {
			bricks.push_back( Brick( i * width, j * height, brickTexture ) );
		}
	}
}

void Game::restartGame()
{
	ball.setX( SCREEN_CENTER_X - ball.getWidth() / 2 );
	ball.setY( paddle.getY() - ball.getHeight() );
	ball.setXVelocity( 0 );
	ball.setYVelocity( 0 );

	for( Brick& brick : bricks ) {
		brick.setIsDead( false );
	}
}

void Game::handleEntityInteractions()
{
	//Check ball out of bounds
	if( ball.getY() + ball.getHeight() >= SCREEN_HEIGHT )
	{
		restartGame();
	}

	//ball hit paddle
	if( checkEntityIntersection( paddle, ball ) )
	{
		if( ball.getYVelocity() > 0 ) //Ball going towards bottom of screen 
		{
			ball.switchYDirection();
		}

		int ballCenterX = ball.getX() + ball.getWidth() / 2;
		int paddleCenterX = paddle.getX() + paddle.getWidth() / 2;
		//Center hits should rebound straight up while hits towards the edge add increasing angle
		ball.setXVelocity( ( ballCenterX - paddleCenterX ) / ( paddle.getWidth() / (MAX_PADDLE_X_REBOUND * 2) ) );
	}

	//ball hit brick
	bool gameWon = true;
	bool brickHit = false;
	for( Brick& brick : bricks ) {
		if( !brick.getIsDead() && checkEntityIntersection( brick, ball ) )
		{
			brickHit = true;
			brick.setIsDead( true );
		}
		if( !brick.getIsDead() ){
			gameWon = false;
		}
	}
	if( brickHit )
		ball.switchYDirection();
	if( gameWon )
		restartGame();
}

void Game::updateEntityLocations()
{
	paddle.update();
	ball.update();
}

void Game::handleMouseEvent()
{
	if( ball.isStationary() )
	{
		ball.setXVelocity( -INIT_BALL_SPEED );
		ball.setYVelocity( -INIT_BALL_SPEED );
	}
}

void Game::drawScene()
{
	gameWindow.clear();
	gameWindow.render( ball );
	gameWindow.render( paddle );
	for( Brick brick : bricks ) {
		if( !brick.getIsDead() )
			gameWindow.render( brick );
	}
	gameWindow.display();
}