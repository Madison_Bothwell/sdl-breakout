#include "paddle.h"
#include "render_window.h"

Paddle::Paddle() :
	Entity( 0, 0, nullptr )
{
}

Paddle::Paddle( int x, int y, SDL_Texture* texture ) :
	Entity( x, y, texture )
{
}

void Paddle::update()
{
	int mouseXPosition;
	SDL_GetMouseState( &mouseXPosition, nullptr );
	setX( mouseXPosition - getWidth() / 2 );
	checkBounds();
}

void Paddle::checkBounds()
{
	if( getX() <= 0 )
	{
		setX( 0 );
	}

	if( getX() + getWidth() >= SCREEN_WIDTH )
	{
		setX( SCREEN_WIDTH - getWidth() );
	}
}