#pragma once
#include <SDL.h>
#include "render_window.h"
#include "entity.h"
#include "ball.h"
#include "paddle.h"
#include "collision.h"
#include "brick.h"
#include <vector>

class Game
{
private:
	RenderWindow& gameWindow;
	Paddle paddle;
	Ball ball;
	std::vector<Brick> bricks;
public:
	Game(RenderWindow& gameWindow);
	void initGame();
	void restartGame();
	void handleEntityInteractions();
	void updateEntityLocations();
	void handleMouseEvent();
	void drawScene();
};

