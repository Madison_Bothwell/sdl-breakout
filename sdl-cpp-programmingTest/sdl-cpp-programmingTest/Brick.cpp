#include "Brick.h"

Brick::Brick() :
	Entity( 0, 0, nullptr ), isDead( false )
{
}

Brick::Brick( int x, int y, SDL_Texture* texture ) :
	Entity( x, y, texture ), isDead( false )
{
}

bool Brick::getIsDead() {
	return isDead;
}

void Brick::setIsDead( bool isDead ) {
	this->isDead = isDead;
}