#pragma once
#include <SDL.h>
#include "entity.h"

class Brick : public Entity
{
public:
	Brick();
	Brick( int x, int y, SDL_Texture* texture );
	bool getIsDead();
	void setIsDead( bool isDead );
private:
	bool isDead;
};
