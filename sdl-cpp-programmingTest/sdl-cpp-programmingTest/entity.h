#pragma once
#include <SDL.h>

class Entity
{
private:
	int x, y;
	SDL_Texture* texture;
	int width, height;
public:
	Entity( int x, int y, SDL_Texture* texture );
	int getX();
	void setX( int x );
	int getY();
	void setY( int y );
	int getWidth();
	int getHeight();
	SDL_Texture* getTexture();
};

