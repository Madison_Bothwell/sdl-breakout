#include "entity.h"
#include <SDL.h>

Entity::Entity( int x, int y, SDL_Texture* texture )
	:x( x ), y( y ), texture( texture )
{
	SDL_QueryTexture( texture, NULL, NULL, &width, &height );
}

int Entity::getX()
{
	return x;
}

void Entity::setX( int x )
{
	this->x = x;
}


int Entity::getY()
{
	return y;
}

void Entity::setY( int y )
{
	this->y = y;
}

int Entity::getWidth()
{
	return width;
}

int Entity::getHeight()
{
	return height;
}

SDL_Texture* Entity::getTexture()
{
	return texture;
}