#include <SDL.h>
#include <stdio.h>
#include "render_window.h"
#include "game.h"

const int SCREEN_TARGET_FPS = 60;
const int SCREEN_TICKS_PER_FRAME = 1000 / SCREEN_TARGET_FPS;

int main( int argc, char* args[] )
{

	// Error check that SDL is initialised correctly
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialise! SDL_Error: %s\n", SDL_GetError() );
	}
	else
	{
		RenderWindow gameWindow = RenderWindow( "Brick Breaker" );
		Game game( gameWindow );
		game.initGame();

		bool quit = false;
		SDL_Event event;

		Uint32 frameStartTicks;
		Uint32 frameEndTicks;

		while( !quit )
		{
			frameStartTicks = SDL_GetTicks();
			while( SDL_PollEvent( &event ) )
			{
				if( event.type == SDL_QUIT )
					quit = true;
				if( event.type == SDL_MOUSEBUTTONDOWN ){
					game.handleMouseEvent();
				}
			}
			
			game.handleEntityInteractions();
			game.updateEntityLocations();
			game.drawScene();

			//Cap the framerate if needed
			frameEndTicks = SDL_GetTicks() - frameStartTicks;
			if( frameEndTicks < SCREEN_TICKS_PER_FRAME )
			{
				//Wait remaining time
				SDL_Delay( SCREEN_TICKS_PER_FRAME - frameEndTicks );
			}
		}

		gameWindow.cleanUp();
	}

	SDL_Quit();
	return 0;
}