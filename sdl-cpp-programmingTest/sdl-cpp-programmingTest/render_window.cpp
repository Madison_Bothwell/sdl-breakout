#include "render_window.h"
#include <stdio.h>

RenderWindow::RenderWindow()
	:window( nullptr ), renderer( nullptr )
{
}

RenderWindow::RenderWindow( const char* windowTitle )
	: window( nullptr ), renderer( nullptr )
{
	window = SDL_CreateWindow( windowTitle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
	if( window == nullptr )
	{
		printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
		SDL_Quit();
	}
	else
	{
		renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
		if( renderer == nullptr )
		{
			printf( "Renderer could not be created! SDL_Error: %s\n", SDL_GetError() );
			cleanUp();
			SDL_Quit();
		}
	}
}

SDL_Texture* RenderWindow::loadTexture( const std::string& file )
{
	SDL_Texture* texture = nullptr;
	SDL_Surface* loadedImage = SDL_LoadBMP( file.c_str() );

	if( loadedImage != nullptr )
	{
		texture = SDL_CreateTextureFromSurface( renderer, loadedImage );
		SDL_FreeSurface( loadedImage );

		if( texture == nullptr )
		{
			printf( "Failed creating texture from surface. SDL_Error: %s\n", SDL_GetError() );
		}
	}
	else
	{
		printf( "Failed loading image for texture. SDL_Error: %s\n", SDL_GetError() );
	}

	return texture;
}

void RenderWindow::renderTexture( SDL_Texture* texture, int x, int y ) {
	SDL_Rect destinationRect;
	destinationRect.x = x;
	destinationRect.y = y;

	SDL_QueryTexture( texture, nullptr, nullptr, &destinationRect.w, &destinationRect.h );
	SDL_RenderCopy( renderer, texture, nullptr, &destinationRect );
}

void RenderWindow::render( Entity& entity ) {
	SDL_Rect destinationRect;
	destinationRect.x = entity.getX();
	destinationRect.y = entity.getY();
	destinationRect.w = entity.getWidth();
	destinationRect.h = entity.getHeight();

	SDL_RenderCopy( renderer, entity.getTexture(), NULL, &destinationRect );
}

void RenderWindow::cleanUp()
{
	SDL_DestroyRenderer( renderer );
	SDL_DestroyWindow( window );
}

void RenderWindow::clear()
{
	SDL_RenderClear( renderer );
}

void RenderWindow::display()
{
	SDL_RenderPresent( renderer );
}