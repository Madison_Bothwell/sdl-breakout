#include "ball.h"
#include "render_window.h"

Ball::Ball() :
	Entity( 0, 0, nullptr ), xVelocity( 0 ), yVelocity( 0 )
{
}

Ball::Ball( int x, int y, SDL_Texture* texture, int xVelocity, int yVelocity ) :
	Entity( x, y, texture ), xVelocity( xVelocity ), yVelocity( yVelocity )
{
}

int Ball::getXVelocity()
{
	return xVelocity;
}

void Ball::setXVelocity( int xVelocity )
{
	this->xVelocity = xVelocity;
}

int Ball::getYVelocity()
{
	return yVelocity;
}

void Ball::setYVelocity( int yVelocity )
{
	this->yVelocity = yVelocity;
}

void Ball::update()
{
	setX( getX() + xVelocity );
	setY( getY() + yVelocity );
	checkBounds();
}

void Ball::checkBounds()
{
	if( getX() <= 0 )
	{
		setX( 0 );
		switchXDirection();
	}

	if( getX() + getWidth() >= SCREEN_WIDTH )
	{
		setX( SCREEN_WIDTH - getWidth() );
		switchXDirection();
	}


	if( getY() <= 0 )
	{
		setY( 0 );
		switchYDirection();
	}

	if( getY() + getHeight() >= SCREEN_HEIGHT )
	{
		setY( SCREEN_HEIGHT - getHeight() );
		switchYDirection();
	}
}

void Ball::switchXDirection()
{
	xVelocity *= -1;
}

void Ball::switchYDirection()
{
	yVelocity *= -1;
}

bool Ball::isStationary()
{
	return xVelocity == 0 && yVelocity == 0;
}