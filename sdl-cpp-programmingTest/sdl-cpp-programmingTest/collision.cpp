#include "collision.h"

bool checkEntityIntersection( Entity& entityA, Entity& entityB )
{
	SDL_Rect enityRectA{ entityA.getX(), entityA.getY(), entityA.getWidth(), entityA.getHeight() };
	SDL_Rect enityRectB{ entityB.getX(), entityB.getY(), entityB.getWidth(), entityB.getHeight() };
	return SDL_HasIntersection( &enityRectA, &enityRectB );
}