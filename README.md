# README #

## Instructions ##

### How to run and play ###
* An executable to play the game can be found here: sdl-breakout/sdl-cpp-programmingTest/x64/Release/game.exe
* Click the mouse to start the game
* The paddle is controlled by your mouse position in the window
* Bounce the ball on the paddle and break all the bricks to window
* The game will restart once you break all the bricks or if the ball falls off the bottom of the board